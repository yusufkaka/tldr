# TLDR
Too Long; Didn't Read (the whole thing, but got the just of it)


**What is TLDR**

TLDR is an app for people like me that don't have the patience to read long articles that are often shared on messaging platforms. Copy the link from your chat and paste into TLDR to get a summary.